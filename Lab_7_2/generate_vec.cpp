#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>

int generation_len_array(){
    int len_array = 1000 + rand() % 9999;
    std::cout << "Generate len array: Done " << len_array << std::endl;
    return len_array;
}

std::vector<int> generate_vec(int len_array){
    std::vector<int> vec;

    for (int i = 0; i < len_array; i++){
       int gen_number = 1 + rand() % 10;
       vec.push_back(gen_number);
    }
    std::cout << "Generate array: Done" << std::endl;
    return vec;
}

void write_vec(std::vector<int> vec){
    std::ofstream out; 
    out.open("./data.txt");
    if (out.is_open())
    {
        for (int i = 0; i <= vec.size(); i++){
            out << vec[i] << std::endl;
        }
    }
    std::cout << "Write array to file data.txt: Done" << std::endl;
}

int main(int argc, char** argv) {
    std::vector<int> vec;
  
    int len_vec = generation_len_array();
    vec = generate_vec(len_vec);
    write_vec(vec);

    return 0;
}