#include <iostream>
#include <cmath>
#include <omp.h>
#include <vector>
#include <fstream>

// Struct for sequences
struct Result {
    int index_i = 0;
    int index_j = 0;
    int len = 1;
};

std::vector<int> read_file_vec(){
    std::vector<int> vec;

    std::string line;
    int count = 0;

    std::ifstream in("./data.txt");
    if (in.is_open())
    {
        while (getline(in, line))
        {
            vec.push_back(atoi(line.c_str()));
        }
    }
    in.close();

    return vec;
}

std::vector<Result> search_sequences(std::vector<int> vec){
    std::vector<Result> vec_sequences; 
    Result result;
 
 #pragma omp parallel for ordered
    for (int i = 0; i < vec.size(); i++) {
        #pragma omp ordered
        {
            for (int j = i + 1; j < vec.size(); j++){
                if (vec[j] > vec[j-1]){
                    result.len++;
                    result.index_i = i;
                    result.index_j = j;
                    vec_sequences.push_back(result);
                }else{
                    result.len = 1;
                    result.index_i = 1;
                    result.index_j = 0;
                    break;
                }
            }
        }
    }
    // Output of found sequences
    for (int i = 0; i < vec_sequences.size(); i++){
        std::cout << "Len: " << vec_sequences[i].len << " Index_i: " << vec_sequences[i].index_i << " Index_j: " << vec_sequences[i].index_j << std::endl;
    }
    return vec_sequences;
}

void search_sequences_max_len(std::vector<Result> vec_sequences){
    Result sequences_with_max_len = {0, 0, 0};
    for (int i = 0; i < vec_sequences.size(); i++){
        if (vec_sequences[i].len > sequences_with_max_len.len){
            sequences_with_max_len = vec_sequences[i];
        }
    }
    std::cout << "Result: " << std::endl;
    std::cout << "Len: " << sequences_with_max_len.len << " Index_i: " << sequences_with_max_len.index_i << " Index_j: " << sequences_with_max_len.index_j << std::endl;
    
}

int main(int argc, char** argv) {
    std::vector<int> vec = read_file_vec();
    std::vector<Result> vec_sequences = search_sequences(vec);
    search_sequences_max_len(vec_sequences);
    return 0;
}