#include <iostream>
#include <ctime>
#include <cassert>
#include <csignal>

#define max(x, y) ( (x) > (y) ? (x) : (y) )

int BP_SIZE = 40;

void backpack (int** d, int* w, int* c, int n)
{
	for (int i = 1; i < n; i++)
	{
		for (int j = 0; j < BP_SIZE; j++)
		{
			d[i][j] = d[i-1][j];
			if (j - w[i] >= 0)
				d[i][j] = max(d[i][j], d[i-1][j-w[i]] + c[i]);
		}
	}
}

void posix_death_signal(int signum)
{
	std::cout << "Seg fault " << std::endl;
	signal(signum, SIG_DFL);
	exit(3);
}

int main(){
	srand(time(NULL));
	signal(SIGSEGV, posix_death_signal);

    int n;
    std::cout << "Enter the number: " << std::endl; 
    std::cin >> n;

	std::cout << "Generating: " << std::endl;
	int * canned_food_weight = (int*)malloc(n * sizeof(int));
	int * canned_food_cal = (int*)malloc(n * sizeof(int));

	for (int i = 0; i < n; i++)
	{
		canned_food_weight[i] = rand() % 5 + 1;
		canned_food_cal[i] = rand() % 1500 + 501;
	}
    int** d = (int**)malloc(sizeof(int*) * n);
    for(int i = 0; i < n; i++)
	{
        d[i] = (int*)malloc(sizeof(int*) * BP_SIZE);
		d[0][i] = 0;
	}
	backpack(d, canned_food_weight, canned_food_cal, n);
	std::cout <<  d[n - 1][BP_SIZE - 1] << std::endl;
	return 0;
}


