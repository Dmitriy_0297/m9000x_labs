#include <mpi.h>
#include <iostream>

int get_magic_number_max(int rank, int size, int magic_number_max, int magic_number){
  if (rank != 0)
  {
    magic_number = rank * rank;
    MPI_Send(&magic_number, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
  } else {
    for (int i = 1; i < size; i++){
      magic_number = 0;
      MPI_Recv(&magic_number, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      if (magic_number > magic_number_max){
        magic_number_max = magic_number;
      }
    }
  }
  return magic_number_max;
}

void output_magic_number_max(int rank, int size, int magic_number_max){
  if (rank == 0) {
    for (int i = 1; i < size; i++){
      MPI_Send(&magic_number_max, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
    }
    } else {
      MPI_Recv(&magic_number_max, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      std::cout << "Process: " << rank <<  ", max magical number: " << magic_number_max << std::endl;
    }
}

int main(int argc, char** argv) {
  int rank, size;
  int magic_number_max = 0;
  int magic_number = 0;

  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  magic_number_max = get_magic_number_max(rank, size, magic_number_max, magic_number);
  output_magic_number_max(rank, size, magic_number_max);

  MPI_Finalize();
  return 0;
}