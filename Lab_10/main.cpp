#include <mpi.h>
#include <iostream>

double *create_array_rand_num(int number_elements){
    double *rand_nums = (double *)malloc(sizeof(double) * number_elements);
    for (int i = 0; i < number_elements; i++){
        double number = (double)(rand())/RAND_MAX;
        rand_nums[i] = number;
    }
    return rand_nums;
}

// result multiplications
double get_multipl(double *array, int num_elem){
    double result = 1.0;
    for (int i = 0; i < num_elem; i++){
        result *= array[i];
    }
    return result;
}

int main(int argc, char** argv){
    int num_elements = atoi(argv[1]);
    int rank, size;
    double *rand_nums;
    
    srand(time(NULL));
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    double *sub_avgs = (double *)malloc(sizeof(double) * size);
    double *sub_rand_nums = (double *)malloc(sizeof(double) * num_elements);

    if (rank == 0) {
        rand_nums = create_array_rand_num(num_elements * size);
    }
    // Scatter a number from one process to another 
    MPI_Scatter(rand_nums, num_elements, MPI_DOUBLE, sub_rand_nums,
              num_elements, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    double sub_avg = get_multipl(sub_rand_nums, num_elements);
    // Gather all partial averages down
    MPI_Gather(&sub_avg, 1, MPI_DOUBLE, sub_avgs, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    // Result
    if (rank == 0){
        double avg = get_multipl(sub_avgs, size);
        std::cout << "Multipl all result elem " << avg << std::endl;

        double original_data_avg = get_multipl(rand_nums, num_elements * size);
        std::cout << "Result multipl original data " << original_data_avg << std::endl;
    }

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}