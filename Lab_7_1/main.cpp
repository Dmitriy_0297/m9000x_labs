#include <iostream>
#include <cmath>
#include <omp.h>

int N = 5;

int main(int argc, char** argv) {
  double a[N];
  double n = 5.0;
  a[0] = 1;
  double w;

  #pragma omp parallel for ordered
  for (int i=1; i<N; i++)
  {
    #pragma omp ordered
    {
    w=sin(i/n*3.14);
    a[i]=w*a[i-1];
    }
  }

  for (int i = 0; i < N; i++) {
        std::cout << a[i] << std::endl;
  }
  return 0;
}
