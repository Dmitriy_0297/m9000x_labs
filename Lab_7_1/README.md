### Задание (14 вариант)
Исправить ошибки в коде:
<code>...
double a[N];
a[0]=1;
double w;
pragma omp parallel for
for (i=1; i<N; i++)
{
    w = sin(i/N*3.14);
    a[i] = w*a[i-1];
}
...</code>

### Запуск программы
1. g++ -c main.cpp -o main.o -fopenmp
2. g++ main.o -o main -fopenmp -lpthread
3. ./main 

