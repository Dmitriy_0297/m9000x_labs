#include <iostream>
#include <omp.h> 
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

int NUMBER_PEPOPLE = 10000;

std::vector<int> read_exparts_to_faculties()
{
    std::vector<int> experts;
    std::string line;
    int count = 0;

    std::ifstream in("./data.txt");
    if (in.is_open())
    {
        while (getline(in, line))
        {
            if (line.find("experts") != std::string::npos){
                  count += line[line.length() - 1] - '0';
                }
            else if (line.find("group") == std::string::npos &&
             line.find("experts") == std::string::npos){
                experts.push_back(count);
                count = 0;
            }
        }
        experts.erase(experts.begin());
    }
    in.close();
    return experts;
}

void calculation_percent_experts_for_faculties(std::vector<int> experts, int number_of_threads){
    int number_of_faculties = experts.size(); //количество факультетов
    int number_of_people_at_the_faculty = NUMBER_PEPOPLE / number_of_faculties; //количество людей на факультете

    #pragma omp parallel for num_threads(number_of_threads)
    for (int i = 0; i < experts.size(); i++){
        std::stringstream stream;
        stream << "faculty_" << i+1 << " - " << (100 * experts[i]) /  number_of_people_at_the_faculty << " % experts" << "\n"; //сделано, что бы избежать critical
        std::cout << stream.str();
    }
}

void calculation_percent_experts_for_university(std::vector<int> experts, int number_of_threads){
    int number_experts_in_university = 0;
    int tmp = 0;

    #pragma omp parallel for num_threads(number_of_threads)
    for (int i = 0; i < experts.size(); i++){
        number_experts_in_university += experts[i];
    }

    std::cout << "UNUVERSITY " << 100 * number_experts_in_university / NUMBER_PEPOPLE << " % experts" << std::endl;
}

int main(int argc, char** argv)
{
    int number_of_threads = atoi(argv[1]);

    std::vector<int> number_of_experts_at_the_faculties;
    number_of_experts_at_the_faculties = read_exparts_to_faculties(); //вектор с количеством знатоков на факультетах

    calculation_percent_experts_for_faculties(number_of_experts_at_the_faculties, number_of_threads);
    calculation_percent_experts_for_university(number_of_experts_at_the_faculties, number_of_threads);
}