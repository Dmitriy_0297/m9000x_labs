#include <iostream>
#include <fstream>
#include <vector>

int NUMBER_PEPOPLE = 10000;

struct foo_return_value {
    int number_of_faculties;
    int number_of_people_at_the_faculty;
    int number_of_groups;
    int number_of_people_at_the_group;
    foo_return_value(int number_of_faculties, int number_of_people_at_the_faculty, int number_of_groups, int number_of_people_at_the_group)
    : number_of_faculties(number_of_faculties), number_of_people_at_the_faculty(number_of_people_at_the_faculty), number_of_groups(number_of_groups), number_of_people_at_the_group(number_of_people_at_the_group) {}
};

foo_return_value generate_numbers(){
    int number_of_faculties = 20 + rand() % 40; // количество факультетов 
    int number_of_groups = 10 + rand() % 30;  // количество групп на факультете
    int number_of_people_at_the_faculty = 0; // количество студентов на факультете
    int number_of_people_at_the_group = 0; // количество студентов в группе


    while (NUMBER_PEPOPLE % number_of_faculties != 0) {
        number_of_faculties = 20 + rand() % 40;
    }
    if (NUMBER_PEPOPLE % number_of_faculties == 0) {
        std::cout << "Количество факультетов: " << number_of_faculties << std::endl;
        int number_of_people_at_the_faculty = NUMBER_PEPOPLE / number_of_faculties;
        std::cout << "Количество людей на факультете: " << number_of_people_at_the_faculty << std::endl;
       
        while (number_of_people_at_the_faculty % number_of_groups != 0) {
            number_of_groups = 10 + rand() % 30;
        }
        if (number_of_people_at_the_faculty % number_of_groups == 0){
            std::cout << "Количество групп на факультете: " << number_of_groups << std::endl;
            int number_of_people_at_the_group = number_of_people_at_the_faculty / number_of_groups;
            std::cout << "Количество людей в одной группе: " << number_of_people_at_the_group << std::endl;
        }
    }
    return foo_return_value(number_of_faculties, number_of_people_at_the_faculty, number_of_groups, number_of_people_at_the_group);
}

void generate_data(int number_of_faculties, int number_of_people_at_the_faculty, int number_of_groups, int number_of_people_at_the_group){
    std::ofstream out; 
    out.open("./data.txt");
    if (out.is_open())
    {
        for (int i = 1; i <= number_of_faculties; i++){
            out << "faculty_" << i << std::endl;
            for (int j = 1; j <= number_of_groups; j++){
                out << "    group_" << j << std::endl;
                out << "        experts: " << 5 + rand() % (number_of_people_at_the_group - 5) << std::endl;
            }
        }
        out << " " << std::endl;
    }
}

int main(int argc, char** argv)
{
    std::cout << "Количество людей в университете: " << NUMBER_PEPOPLE << std::endl;
    foo_return_value generate_value =  generate_numbers();
    generate_data(generate_value.number_of_faculties, generate_value.number_of_people_at_the_faculty,
    generate_value.number_of_groups, generate_value.number_of_people_at_the_group);
    return 0;
}
